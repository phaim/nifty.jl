using NIFTy
using Documenter

makedocs(;
    modules=[NIFTy],
    authors="Philipp Haim",
    repo="https://gitlab.com/Phypix/NIFTy.jl/blob/{commit}{path}#L{line}",
    sitename="NIFTy.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://Phypix.gitlab.io/NIFTy.jl",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
    ],
)
