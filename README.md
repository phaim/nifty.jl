# NIFTy

[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://Phypix.gitlab.io/NIFTy.jl/dev)
[![Build Status](https://gitlab.com/Phypix/NIFTy.jl/badges/master/pipeline.svg)](https://gitlab.com/Phypix/NIFTy.jl/pipelines)
[![Coverage](https://gitlab.com/Phypix/NIFTy.jl/badges/master/coverage.svg)](https://gitlab.com/Phypix/NIFTy.jl/commits/master)
[![Code Style: Blue](https://img.shields.io/badge/code%20style-blue-4495d1.svg)](https://github.com/invenia/BlueStyle)
