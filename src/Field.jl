struct Field
    domain::Domain
    val
end

function getindex(field::Field, inds::UInt...)
    return field.val[inds...]
end

function setindex!(field::Field, X, inds::UInt...)
    field.val[inds...] = X
end
