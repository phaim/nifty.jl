module NIFTy

struct Field
    Domain::AbstractDomain
    val
end

abstract type AbstractDomain
    size
end

struct StructuredDomain <: AbstractDomain
    harmonic::Bool
end
struct RGDomain <: StructuredDomain
end

end
